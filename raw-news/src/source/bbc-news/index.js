import fetch from "node-fetch";



const get = url =>
    fetch(url)
        .then(x => x.text());


export const feeds = [
    {
        _id:'bbc:top',
        source: "BBC News",
        title: "Top Stories",
        content: () => get("http://feeds.bbci.co.uk/news/rss.xml")
    },
    {
        _id:'bbc:world',
        source: "BBC News",
        title: "World",
        content: () => get("http://feeds.bbci.co.uk/news/world/rss.xml")
    },
    {
        _id:'bbc:uk',
        source: "BBC News",
        title: "UK",
        content: () => get("http://feeds.bbci.co.uk/news/uk/rss.xml")
    },
    {
        _id:'bbc:business',
        source: "BBC News",
        title: "Business",
        content: () => get("http://feeds.bbci.co.uk/news/business/rss.xml")
    },
    {
        _id:'bbc:politics',
        source: "BBC News",
        title: "Politics",
        content: () => get("http://feeds.bbci.co.uk/news/politics/rss.xml")
    },
    {
        _id:'bbc:health',
        source: "BBC News",
        title: "Health",
        content: () => get("http://feeds.bbci.co.uk/news/health/rss.xml")
    },
    {
        _id:'bbc:edfam',
        source: "BBC News",
        title: "Education & Family",
        content: () => get("http://feeds.bbci.co.uk/news/education/rss.xml")
    },
    {
        _id:'bbc:scienv',
        source: "BBC News",
        title: "Science & Environment",
        content: () => get("http://feeds.bbci.co.uk/news/science_and_environment/rss.xml")
    },
    {
        _id:'bbc:technology',
        source: "BBC News",
        title: "Technology",
        content: () => get("http://feeds.bbci.co.uk/news/technology/rss.xml")
    },
    {
        _id:'bbc:entart',
        source: "BBC News",
        title: "Entertainment & Arts",
        content: () => get("http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml")
    },

    {
        _id:'bbc:africa',
        source: "BBC News",
        title: "Africa",
        content: () => get("http://feeds.bbci.co.uk/news/world/africa/rss.xml")
    },
    {
        _id:'bbc:asia',
        source: "BBC News",
        title: "Asia",
        content: () => get("http://feeds.bbci.co.uk/news/world/asia/rss.xml")
    },
    {
        _id:'bbc:europe',
        source: "BBC News",
        title: "Europe",
        content: () => get("http://feeds.bbci.co.uk/news/world/europe/rss.xml")
    },
    {
        _id:'bbc:latame',
        source: "BBC News",
        title: "Latin America",
        content: () => get("http://feeds.bbci.co.uk/news/world/latin_america/rss.xml")
    },
    {
        _id:'bbc:mideas',
        source: "BBC News",
        title: "Middle East",
        content: () => get("http://feeds.bbci.co.uk/news/world/middle_east/rss.xml")
    },
    {
        _id:'bbc:uscan',
        source: "BBC News",
        title: "US & Canada",
        content: () => get("http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml")
    },
    {
        _id:'bbc:england',
        source: "BBC News",
        title: "England",
        content: () => get("http://feeds.bbci.co.uk/news/england/rss.xml")
    },
    {
        _id:'bbc:norire',
        source: "BBC News",
        title: "Northern Ireland",
        content: () => get("http://feeds.bbci.co.uk/news/northern_ireland/rss.xml")
    },
    {
        _id:'bbc:scotland',
        source: "BBC News",
        title: "Scotland",
        content: () => get("http://feeds.bbci.co.uk/news/scotland/rss.xml")
    },
    {
        _id:'bbc:wales',
        source: "BBC News",
        title: "Wales",
        content: () => get("http://feeds.bbci.co.uk/news/wales/rss.xml")
    }
];