import {expect} from "chai"

import {feeds} from "../src"


describe('feeds', () => {
    it('all _ids should be unique', () => {
        const ids =
            feeds.reduce((x, y) => x.add(y._id), new Set());

        expect(ids.size).to.equal(feeds.length);
    });

    it('all feeds have an _id, source, title and content', () => {
        for(const feed of feeds) {
            expect(feed._id).to.be.a('string').that.is.not.empty;
            expect(feed.source).to.be.a('string').that.is.not.empty;
            expect(feed.title).to.be.a('string').that.is.not.empty;
            expect(feed.content).to.be.a('function');
        }
    });
});
