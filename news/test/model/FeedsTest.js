import {expect} from "chai"

import * as Feeds from "../../src/model/Feeds"


describe('update', () => {
    it('no feeds in the repository result in all supplied feeds being inserted', () => {
        let insertedFeeds =
            null;

        let fullFeeds =
            null;

        const repository = {
            feeds: () => Promise.resolve([]),
            insertFeeds: feeds => {
                insertedFeeds = feeds;
                return Promise.resolve({"acknowledged": true, "insertedIds": feeds.map(x => x._id)});
            }
        };

        const feedsToInsert = [
            {
                _id: 'bbc:business',
                source: "BBC Feeds",
                title: "Business",
                content: () => get("http://feeds.bbci.co.uk/news/business/rss.xml")
            },
            {
                _id: 'bbc:politics',
                source: "BBC Feeds",
                title: "Politics",
                content: () => get("http://feeds.bbci.co.uk/news/politics/rss.xml")
            }
        ];

        return Feeds.update(repository, feedsToInsert)
            .then(f => fullFeeds = f)
            .catch(console.error)
            .finally(() => {
                expect(insertedFeeds.length).to.equal(2);
                expect(fullFeeds.length).to.equal(2);
            });
    });

    it('a single feed in the repository results in only the new supplied feeds being inserted', () => {
        let insertedFeeds =
            null;

        let fullFeeds =
            null;

        const existingFeeds = [
            {
                _id: 'bbc:business',
                source: "BBC Feeds",
                title: "Business",
                content: () => get("http://feeds.bbci.co.uk/news/business/rss.xml")
            }
        ];

        const repository = {
            feeds: () => Promise.resolve(existingFeeds),
            insertFeeds: feeds => {
                insertedFeeds = feeds;
                return Promise.resolve({"acknowledged": true, "insertedIds": feeds.map(x => x._id)});
            }
        };

        const feedsToInsert = [
            {
                _id: 'bbc:politics',
                source: "BBC Feeds",
                title: "Politics",
                content: () => get("http://feeds.bbci.co.uk/news/politics/rss.xml")
            }
        ];

        return Feeds.update(repository, existingFeeds.concat(feedsToInsert))
            .then(f => fullFeeds = f)
            .catch(console.error)
            .finally(() => {
                expect(insertedFeeds.length).to.equal(1);
                expect(insertedFeeds[0]._id).to.equal('bbc:politics');
                expect(insertedFeeds[0].source).to.equal('BBC Feeds');
                expect(insertedFeeds[0].title).to.equal('Politics');

                expect(fullFeeds.length).to.equal(2);
            });
    });

    it('values in the configured feeds overwrites what is held within the repository', () => {
        let insertedFeeds =
            null;

        let fullFeeds =
            null;

        const existingFeeds = [
            {
                _id: 'bbc:business',
                source: "BBC Feeds Old",
                title: "Business",
                onlyInOld: 'yes',
                content: () => get("http://feeds.bbci.co.uk/news/business/rss.xml")
            }
        ];

        const repository = {
            feeds: () => Promise.resolve(existingFeeds),
            insertFeeds: feeds => {
                insertedFeeds = feeds;
                return Promise.resolve({"acknowledged": true, "insertedIds": feeds.map(x => x._id)});
            }
        };

        const feedsToInsert = [
            {
                _id: 'bbc:business',
                source: "BBC Feeds New",
                title: "Business",
                onlyInNew: 'yes',
                content: () => get("http://feeds.bbci.co.uk/news/business/rss.xml")
            }
        ];

        return Feeds.update(repository, feedsToInsert)
            .then(f => fullFeeds = f)
            .catch(console.error)
            .finally(() => {
                expect(insertedFeeds.length).to.equal(0);

                expect(fullFeeds.length).to.equal(1);
                expect(fullFeeds[0]._id).to.equal('bbc:business');
                expect(fullFeeds[0].source).to.equal('BBC Feeds New');
                expect(fullFeeds[0].title).to.equal('Business');
                expect(fullFeeds[0].onlyInOld).to.equal('yes');
                expect(fullFeeds[0].onlyInNew).to.equal('yes');
            });
    });
});


describe('updateFeeds', () => {
    const rawRssData=
        '<?xml version="1.0" encoding="UTF-8"?>\n' +
        '<?xml-stylesheet title="XSL_formatting" type="text/xsl" href="/shared/bsp/xsl/rss/nolsol.xsl"?>\n' +
        '<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0" xmlns:media="http://search.yahoo.com/mrss/">\n' +
        '    <channel>\n' +
        '        <title><![CDATA[BBC News - Home]]></title>\n' +
        '        <description><![CDATA[BBC News - Home - Description]]></description>\n' +
        '        <link>https://www.bbc.co.uk/news/</link>\n' +
        '        <image>\n' +
        '            <url>https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif</url>\n' +
        '            <title>BBC News - Home</title>\n' +
        '            <link>https://www.bbc.co.uk/news/</link>\n' +
        '        </image>\n' +
        '        <generator>RSS for Node</generator>\n' +
        '        <lastBuildDate>Wed, 16 Oct 2019 09:58:33 GMT</lastBuildDate>\n' +
        '        <copyright><![CDATA[Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.]]></copyright>\n' +
        '        <language><![CDATA[en-gb]]></language>\n' +
        '        <ttl>15</ttl>\n' +
        '        <item>\n' +
        "            <title><![CDATA[Harry Dunn's parents reject Trump offer to meet suspect at White House]]></title>\n" +
        `            <description><![CDATA[They felt "ambushed" by Donald Trump's offer to meet suspect Anne Sacoolas at the White House.]]></description>\n` +
        '            <link>https://www.bbc.co.uk/news/uk-england-northamptonshire-50064595</link>\n' +
        '            <guid isPermaLink="true">https://www.bbc.co.uk/news/uk-england-northamptonshire-50064595</guid>\n' +
        '            <pubDate>Wed, 16 Oct 2019 09:24:25 GMT</pubDate>\n' +
        '        </item>\n' +
        '        <item>\n' +
        '            <title><![CDATA[Brexit: Talks enter last day before crunch EU summit]]></title>\n' +
        '            <description><![CDATA[Brexit negotiations enter a key 24 hours, as EU leaders prepare to meet at a summit in Brussels.]]></description>\n' +
        '            <link>https://www.bbc.co.uk/news/uk-politics-50063022</link>\n' +
        '            <guid isPermaLink="true">https://www.bbc.co.uk/news/uk-politics-50063022</guid>\n' +
        '            <pubDate>Wed, 16 Oct 2019 09:27:05 GMT</pubDate>\n' +
        '        </item>\n' +
        '        <item>\n' +
        '            <title><![CDATA[Have UK voters changed their minds on Brexit?]]></title>\n' +
        '            <description><![CDATA[Where do voters stand on Brexit, after more than three years of debate and negotiation?]]></description>\n' +
        '            <link>https://www.bbc.co.uk/news/uk-politics-50043549</link>\n' +
        '            <guid isPermaLink="true">https://www.bbc.co.uk/news/uk-politics-50043549</guid>\n' +
        '            <pubDate>Tue, 15 Oct 2019 23:22:30 GMT</pubDate>\n' +
        '        </item>\n' +

        '    </channel>\n' +
        '</rss>';

    it('insert new articles in a feed', () => {
        const feeds = [
            {
                _id: 'bbc:business',
                source: "BBC Feeds Old",
                title: "Business",
                onlyInOld: 'yes',
                content: () => Promise.resolve(rawRssData)
            }
        ];

        const articles =
            [];

        const repository = {
            updateFeed: f => Promise.resolve(f),
            updateArticle: a => {
                articles.push(a)
            }
        };

        return Feeds.updateFeeds(repository, feeds)
            .finally(() => {
                expect(articles.length).to.equal(3);
            });
    })
});