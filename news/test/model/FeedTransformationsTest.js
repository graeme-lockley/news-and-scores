import {expect} from "chai"
import {parseRSS} from "../../src/model/FeedTransformations";


describe('FeedTransformation', () => {
    it('\'RSS Feed\'', () => {
        const rssFeed =
            '<?xml version="1.0" encoding="UTF-8"?>\n' +
            '<?xml-stylesheet title="XSL_formatting" type="text/xsl" href="/shared/bsp/xsl/rss/nolsol.xsl"?>\n' +
            '<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0" xmlns:media="http://search.yahoo.com/mrss/">\n' +
            '    <channel>\n' +
            '        <title><![CDATA[BBC News - Home]]></title>\n' +
            '        <description><![CDATA[BBC News - Home - Description]]></description>\n' +
            '        <link>https://www.bbc.co.uk/news/</link>\n' +
            '        <image>\n' +
            '            <url>https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif</url>\n' +
            '            <title>BBC News - Home</title>\n' +
            '            <link>https://www.bbc.co.uk/news/</link>\n' +
            '        </image>\n' +
            '        <generator>RSS for Node</generator>\n' +
            '        <lastBuildDate>Wed, 16 Oct 2019 09:58:33 GMT</lastBuildDate>\n' +
            '        <copyright><![CDATA[Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.]]></copyright>\n' +
            '        <language><![CDATA[en-gb]]></language>\n' +
            '        <ttl>15</ttl>\n' +
            '        <item>\n' +
            "            <title><![CDATA[Harry Dunn's parents reject Trump offer to meet suspect at White House]]></title>\n" +
            `            <description><![CDATA[They felt "ambushed" by Donald Trump's offer to meet suspect Anne Sacoolas at the White House.]]></description>\n` +
            '            <link>https://www.bbc.co.uk/news/uk-england-northamptonshire-50064595</link>\n' +
            '            <guid isPermaLink="true">https://www.bbc.co.uk/news/uk-england-northamptonshire-50064595</guid>\n' +
            '            <pubDate>Wed, 16 Oct 2019 09:24:25 GMT</pubDate>\n' +
            '        </item>\n' +
            '        <item>\n' +
            '            <title><![CDATA[Brexit: Talks enter last day before crunch EU summit]]></title>\n' +
            '            <description><![CDATA[Brexit negotiations enter a key 24 hours, as EU leaders prepare to meet at a summit in Brussels.]]></description>\n' +
            '            <link>https://www.bbc.co.uk/news/uk-politics-50063022</link>\n' +
            '            <guid isPermaLink="true">https://www.bbc.co.uk/news/uk-politics-50063022</guid>\n' +
            '            <pubDate>Wed, 16 Oct 2019 09:27:05 GMT</pubDate>\n' +
            '        </item>\n' +
            '        <item>\n' +
            '            <title><![CDATA[Have UK voters changed their minds on Brexit?]]></title>\n' +
            '            <description><![CDATA[Where do voters stand on Brexit, after more than three years of debate and negotiation?]]></description>\n' +
            '            <link>https://www.bbc.co.uk/news/uk-politics-50043549</link>\n' +
            '            <guid isPermaLink="true">https://www.bbc.co.uk/news/uk-politics-50043549</guid>\n' +
            '            <pubDate>Tue, 15 Oct 2019 23:22:30 GMT</pubDate>\n' +
            '        </item>\n' +

            '    </channel>\n' +
            '</rss>';


        const feed =
            parseRSS(rssFeed);

        expect(feed.channel.title).to.equal("BBC News - Home");
        expect(feed.channel.description).to.equal("BBC News - Home - Description");
        expect(feed.channel.link).to.equal("https://www.bbc.co.uk/news/");
        expect(feed.channel.copyright).to.equal("Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.");
        expect(feed.channel.buildDate).to.equal(1571219913000);
        expect(feed.channel.image.url).to.equal("https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif");
        expect(feed.channel.image.title).to.equal("BBC News - Home");
        expect(feed.channel.image.link).to.equal("https://www.bbc.co.uk/news/");

        expect(feed.articles.length).to.equal(3);

        expect(feed.articles[0]._id).to.equal('50064595');
        expect(feed.articles[0].title).to.equal('Harry Dunn\'s parents reject Trump offer to meet suspect at White House');
        expect(feed.articles[0].description).to.equal('They felt "ambushed" by Donald Trump\'s offer to meet suspect Anne Sacoolas at the White House.');
        expect(feed.articles[0].url).to.equal('https://www.bbc.co.uk/news/uk-england-northamptonshire-50064595');
        expect(feed.articles[0].permanent).to.equal(true);
        expect(feed.articles[0].publishedDate).to.equal(1571217865000);
    });
});
