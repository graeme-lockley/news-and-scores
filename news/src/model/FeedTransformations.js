import {parse} from 'fast-xml-parser';


const extractLastNumber = text => {
    let index =
        text.length - 1;

    while (index > 0 && '0123456789'.indexOf(text[index]) !== -1) {
        index -= 1;
    }

    return text.slice(index + 1);
};


export const parseRSS = feed => {
    const message = parse(feed, {
        attributeNamePrefix: "@_",
        attrNodeName: true,
        textNodeName: "#text",
        ignoreAttributes: false,
        ignoreNameSpace: false,
        allowBooleanAttributes: false,
        parseNodeValue: true,
        parseAttributeValue: false,
        trimValues: true,
        cdataTagName: false, //default is 'false'
        cdataPositionChar: "\\c"
    });

    const result = {
        channel: {
            title: message.rss.channel.title,
            description: message.rss.channel.description,
            link: message.rss.channel.link,
            copyright: message.rss.channel.copyright,
            buildDate: new Date(Date.parse(message.rss.channel.lastBuildDate)).getTime(),
            runDate: new Date().getTime()
        },
        articles:
            message.rss.channel.item.map(item => ({
                _id: extractLastNumber(item.guid['#text']),
                title: item.title,
                description: item.description,
                url: item.link,
                permanent: item.guid['true']['@_isPermaLink'] !== 'false',
                publishedDate: new Date(Date.parse(item.pubDate)).getTime()
            }))
    };

    if (message.rss.channel.image !== undefined) {
        const image =
            message.rss.channel.image;

        result.channel.image = {};

        if (image.url !== undefined) {
            result.channel.image.url = image.url;
        }
        if (image.title !== undefined) {
            result.channel.image.title = image.title;
        }
        if (image.link !== undefined) {
            result.channel.image.link = image.link;
        }
    }

    return result;
};