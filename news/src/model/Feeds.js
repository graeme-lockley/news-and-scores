import * as FT from "./FeedTransformations"

const feedsToIDMap = feeds =>
    feeds.reduce((map, item) => map.set(item._id, item), new Map());


const mergeFeed = (persistentFeed, configuredFeed) =>
    persistentFeed === undefined
        ? configuredFeed
        : Object.assign({}, persistentFeed, configuredFeed);


// load :: (Mongo:DB, List<RawNews:Feed>) -> Promise<Mongo:Error, List<Model:Feed>>
export const update = (repository, feeds) =>
    repository.feeds()
        .then(feedsInRepository => {
            const feedsInRepositoryOnID =
                feedsToIDMap(feedsInRepository);

            const feedsToInsert =
                feeds.filter(feed => !feedsInRepositoryOnID.has(feed._id)).map(f => ({
                    _id: f._id,
                    source: f.source,
                    title: f.title
                }));

            return repository.insertFeeds(feedsToInsert)
                .then(() => feeds.map(feed => mergeFeed(feedsInRepositoryOnID.get(feed._id), feed)));
        });


const toUpdate = feed =>
    true;


const transformItems = content =>
    FT.parseRSS(content);

const updateFeed = (repository, feed) =>
    feed.content()
        .then(content => {
            const parsedContent =
                transformItems(content);

            feed.link = parsedContent.channel.link;
            feed.copyright = parsedContent.channel.copy;
            feed.buildDate = parsedContent.channel.buildDate;
            feed.runDate = parsedContent.channel.runDate;
            feed.image = parsedContent.channel.image;

            return repository.updateFeed(feed)
                .then(() => {
                    parsedContent.articles.forEach(article => {
                        article._id = feed._id + ':' + article._id;
                        article._feedID = feed._id;
                    });

                    return Promise.all(parsedContent.articles.map(article => repository.updateArticle(article)));
                });
        });


export const updateFeeds = (repository, feeds) =>
    Promise.all(
        feeds
            .filter(feed => toUpdate(feed))
            .map(feed => updateFeed(repository, feed))
    );