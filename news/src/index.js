import {feeds} from "../../../raw-news/dist/src"

import * as Feeds from "./model/Feeds"
import * as Repository from "./port/Repository"


Repository.connect()
    .then(repository =>
        Feeds.update(repository, feeds)
            .then(feeds => Feeds.updateFeeds(repository, feeds))
            .finally(() => repository.close()))
    .catch(console.error);


