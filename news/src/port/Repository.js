import {MongoClient} from "mongodb";


const client = () =>
    new Promise((resolve, reject) => {
        try {
            resolve(new MongoClient("mongodb://localhost:27017/thepolyglotdeveloper", {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }));
        } catch (e) {
            reject(e);
        }
    });


const find = (collection, query) =>
    new Promise((resolve, reject) => {
        collection.find(query).toArray((err, docs) => {
            if (err === null)
                resolve(docs);
            else
                reject(err);
        })
    });


const findOne = (collection, query) =>
    new Promise((resolve, reject) => {
        try {
            resolve(collection.findOne(query));
        } catch (e) {
            reject(e);
        }
    });


// insertMany :: (Mongo:Collection, List<X>) -> Promise(Mongo:Error, Mongo:InsertManyResult)
const insertMany = (collection, docs) =>
    new Promise((resolve, reject) => {
        try {
            resolve(collection.insertMany(docs));
        } catch (e) {
            reject(e);
        }
    });


// feeds :: Mongo:DB -> Promise<Mongo:Error, List<{_id :: String, ..}>>
const feeds = db =>
    find(db.collection('feeds'), {});


// insertFeeds :: (Mongo:DB, List<{}>) -> Promise<Mongo:Error, Mongo:InsertManyResult>
const insertFeeds = (db, feeds) =>
    feeds.length === 0
        ? Promise.resolve(({"acknowledged": true, "insertedIds": []}))
        : insertMany(db.collection('feeds'), feeds);


const updateFeed = (db, feed) =>
    new Promise((resolve, reject) => {
        try {
            db.collection('feeds').replaceOne({_id: feed._id}, feed);

            resolve(feed);
        } catch (e) {
            reject(e);
        }
    });


const updateArticle = (db, article) =>
    new Promise((resolve, reject) => {
        try {
            db.collection('articles').replaceOne({_id: article._id}, article, {upsert: true});

            resolve(article);
        } catch (e) {
            reject(e);
        }
    });


// connect :: () -> Promise<Mongo:Error, { feeds :: () -> Promise<Mongo:Error, List<{_id :: String, ..}>>, insertFeeds :: List<{}> -> Promise<Mongo:Error, Mongo:InsertManyResult> }>
export const connect = () =>
    client()
        .then(client =>
            new Promise((resolve, reject) => {
                client.connect(err => {
                    if (err === null) {
                        resolve(client.db());
                    } else {
                        reject(err);
                    }
                });
            }).then(db => ({
                    feeds: () => feeds(db),
                    insertFeeds: f => insertFeeds(db, f),
                    updateFeed: f => updateFeed(db, f),
                    updateArticle: a => updateArticle(db, a),
                    close: () => client.close()
                })
            )
        );
