# news-and-scores

Over the years I have picked up my news firstly from sites and then apps.  
Equally whenever I have wanted to pick up the latest score of one of the sports
that I am interested in I have needed to look across numerous web sites and apps
to keep myself up-to-date.

A number of things are really annyoing to me:

*  News applications are littered with advertisements and the advertisements do horrid things - occupy screen estate, cause the page to be reformatted as the advertisement detail comes through and consumes my bandwidth.
*  News aggregators change their criteria with respect to relevance.
*  News aggregator apps are bought and/or replaced.
*  RSS agregators don't hit the mark.
*  Scores for rugby union, cricket, golf, football and F1 are all over the place.

Finally the single most annoying thing is that this has all been monitised.

This project is my response to the above with the following objectives:

* I would want to have my news in the format that I want
* I want to read my news without distraction
* I want to follow my sports and only my sports
* I don't want to be pestered to buy anything
* I don't want to be told anything that I am not looking at